package ssm.demo1.dao;
import org.springframework.data.repository.query.Param;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ssm.demo1.entity.Student;

import java.util.List;

public interface StudentDao extends JpaRepository<Student,Integer> {
    Student findStudentById(Integer id);
    @Query(name="findStuByName",nativeQuery = true,value="select * from student where name=:name")
    List<Student> findStuByName(@Param("name") String name);

    @Query(name="save",nativeQuery =true,value="insert into student(id,name,age,sex)values(1234,'Wang',22,'man');")
    Student save(@Param("id")int id,@Param("name")String name,@Param("age")int age,@Param("sex")String sex);
}
