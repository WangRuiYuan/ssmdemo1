package ssm.demo1.dao;
import org.springframework.data.repository.query.Param;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ssm.demo1.entity.User;

public interface UserDao extends JpaRepository<User,Integer> {
    User findUserById(Integer id);

    @Query(name="login",nativeQuery = true,value = "select * from user where username=:username and password=:password")
    User login(@Param("username")String username,@Param("password")String password);
}
