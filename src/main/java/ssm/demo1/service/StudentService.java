package ssm.demo1.service;

import org.springframework.data.domain.Page;
import ssm.demo1.dao.StudentDao;
import ssm.demo1.entity.Student;

import java.util.List;

public interface StudentService {
    Student save(Student student);
    Student update(Student student);
    void delete(Integer id);
    Student findStuById(Integer id);
   List<Student> findStuByName(String name);
  /* Page<StudentDao> findAll(int page, int pageSize);*/
}
