package ssm.demo1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import ssm.demo1.entity.Student;
import ssm.demo1.service.StudentService;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/Student")
public class StudentController {
   @Autowired
    private StudentService studentService;

    @RequestMapping("/add")
    public Student save(Student student){
       return studentService.save(student);
   }
   @PostMapping("/update")
    public Student update(Student student){
       return studentService.update(student);
   }
   @GetMapping("/del/{id}")
    public String del(@PathVariable int id){
       studentService.delete(id);
       return "yes";
   }
   @GetMapping("/findByName/{name}")
    public List<Student> findByName(@PathVariable String name){
       return studentService.findStuByName(name);
   }
  /* @GetMapping("/query")
   public Page<Student> findByPage(Integer page, HttpServletResponse response){
       response.setHeader("Access-Control-Allow-Origin","*");
       if (page==null||page<=0){
           page=0;
       }
       else {
           page-=1;
       }
       return studentService.findAll(page,5);
   }*/
}
