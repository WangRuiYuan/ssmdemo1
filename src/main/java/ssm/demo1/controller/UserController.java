package ssm.demo1.controller;
import org.springframework.data.repository.query.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ssm.demo1.entity.User;
import ssm.demo1.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/User")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/findAll")
    public List<User>findAll(){
        return userService.findAll();
    }
    @RequestMapping("/query")
    public User findById(Integer id){
        return userService.findUserById(id);
    }
    @RequestMapping("/reg")
    public User reg(User user){
        return userService.save(user);
    }
    @RequestMapping("/login" )
    public User login(String username,String password){
        return userService.login(username,password);
    }
}
